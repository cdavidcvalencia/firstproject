﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GoPlay : MonoBehaviour, IPointerClickHandler{

    public Animator playAnim;
	float timer = 0f;
	float maxTimer = 1f;
	bool countToLoad=false;

	public void CustomUpdate(){
		if (countToLoad) {
			if (timer < maxTimer) {
				timer += Time.deltaTime;
			} else {
				StageController.instance.LoadStage ();
				countToLoad = false;
			}
		}
	}

    public void OnPointerClick(PointerEventData eventData){
        playAnim.SetTrigger ("click");
		countToLoad = true;
		timer = 0;
    }

}
