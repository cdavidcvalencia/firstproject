﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeManager : MonoBehaviour {

    void Start () {
        LoadEverything ();
    }

    void LoadEverything(){
        Transform panel= (Instantiate(Resources.Load("Home/UI", typeof(GameObject))) as GameObject).transform.Find("Panel");
        Transform playBtn = panel.Find ("PlayBtn");
        Animator playAnim = playBtn.GetComponent<Animator> ();
        playBtn.GetComponent<GoPlay> ().playAnim = playAnim;
        panel.Find ("Logo").GetComponent<GoPlay> ().playAnim = playAnim;
    }

}
