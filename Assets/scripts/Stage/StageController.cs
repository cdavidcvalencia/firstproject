﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageController : MonoBehaviour {

    public static int level=2;
    public static StageController instance;

    GameObject floorPrefab;
    GameObject[,] floorTiles = new GameObject[1, 1];

    PlayerBody playerBody;

    float startTime;
    public Text text;
    int levelMultiplier = 1;

	Transform panel;

	bool stageLoaded=false;

	GoPlay[] goPlay = new GoPlay[2]; 

    void Start () {
        instance=this;
		LoadHome (); 
    }

	public void LoadStage(){
		stageLoaded = true;
		Destroy (panel.gameObject);
		panel = null;
		LoadStageResources ();
		startTime=Time.time;
		CreateStage();
	}

	void LoadHome(){
		panel= (Instantiate(Resources.Load("Home/UI", typeof(GameObject))) as GameObject).transform.Find("Panel");
		Transform playBtn = panel.Find ("PlayBtn");
		Animator playAnim = playBtn.GetComponent<Animator> ();
		goPlay[0] = playBtn.GetComponent<GoPlay> ();
		goPlay[0].playAnim = playAnim;
		goPlay[1] = panel.Find ("Logo").GetComponent<GoPlay> ();
		goPlay[1].playAnim = playAnim;
	}

    void LoadStageResources(){
        GameObject playerBodyGO=Instantiate (Resources.Load ("Game/PlayerBody", typeof(GameObject))) as GameObject;
        playerBodyGO.transform.position = Vector3.zero;
        playerBodyGO.name = "Player Body";
        playerBody = playerBodyGO.GetComponent<PlayerBody> ();
		playerBody.Initialize ();
        playerBody.cameraTransform = transform;
        Globals.playerBody = playerBodyGO;
        Globals.Initialize();
        text=(Instantiate (Resources.Load ("Game/Canvas", typeof(GameObject))) as GameObject).transform.Find("Text").GetComponent<Text>();
        floorPrefab = Resources.Load ("Game/Floor", typeof(GameObject)) as GameObject;
        MonsterManager.monsterPrefab = Resources.Load ("Game/Enemy", typeof(GameObject)) as GameObject;
		MonsterManager.monsterPrefabMedium = Resources.Load ("Game/Enemy_medium", typeof(GameObject)) as GameObject;
		MonsterManager.monsterPrefabSmall = Resources.Load ("Game/Enemy_small", typeof(GameObject)) as GameObject;
        EggManager.eggPrefab= Resources.Load ("Game/Egg", typeof(GameObject)) as GameObject;
    }

    void CreateStage()
    {
        int size = level * 2 * levelMultiplier;
        float delta = -((level * levelMultiplier) - 0.5f);
        for (int x = 0; x < floorTiles.Length; x++)
        {
            for (int y = 0; y < floorTiles.Length; y++)
            {
                Destroy(floorTiles[x, y]);
            }
        }
        MonsterManager.Initialize (level,levelMultiplier,size);
        GameObject floor;
        floorTiles = new GameObject[size,size];
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                floor=Instantiate(floorPrefab);
                floor.transform.position = new Vector3(delta + x, delta + y, 0);
                floorTiles[x,y]=floor;
            }
        }
    }

    void UpdateTimer(){
        string time="";
        float currentTime = (Time.time - startTime);
        long currentSeconds = (long)currentTime;
        long currentMinutes = 0;
        if (currentSeconds > 59) {
            currentMinutes = currentSeconds / 60;
            currentSeconds -= currentMinutes * 60;
        }
        time += ((currentSeconds < 10) ? "0" : "") + currentSeconds;
        if (currentMinutes > 0) {
            time = ((currentMinutes < 10) ? "0" : "") + currentMinutes+":"+time;
        }
        text.text = time;
    }

	void FixedUpdate(){
		if(!stageLoaded){
			return;
		}
		playerBody.CustomFixedUpdate ();
	}

    void Update () {
		if(!stageLoaded){
			for (var i = 0; i < 2; i++) {
				goPlay[i].CustomUpdate ();
			}
			return;
		}
        UpdateTimer ();
        MonsterManager.CustomUpdate ();
        EggManager.CustomUpdate ();
    }

}