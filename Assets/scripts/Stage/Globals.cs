﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals {

    public static GameObject playerBody;
    public static float layEggChance;

    public static void Initialize(){
        layEggChance=StageController.level;
    }

    public static void EggLayed(){
        if (layEggChance>0.5f) {
            layEggChance -= 1f/StageController.level;
        }
        if (layEggChance<0.5f) {
            layEggChance = 0.5f;
        }
    }

}