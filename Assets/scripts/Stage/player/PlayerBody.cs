﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBody : MonoBehaviour {

    float speed = 0.5F;
    public Transform cameraTransform;
	Vector3 walkTarget=new Vector3(0,0,0);
    Quaternion targetRotation;
    byte lives=13;
    Animator anim;
    Animator maskAnim;
    bool alive = true;
    bool walking=false;
    bool[] tentaclesHurt;
    Animator[] tentacles;
    float nextBlink;
    float timeBeforeBlink;
	Transform cuerpo;

	public Vector2 GetMouseVector(){
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		Vector2 origin = new Vector2(ray.origin.x,ray.origin.y);
		Vector2 direction = new Vector2(ray.direction.x,ray.direction.y);
		RaycastHit2D info=Physics2D.Raycast (origin, direction);
		Vector2 point = info.point;
		if (info.collider != null) {
			Vector3 floorPos = info.collider.transform.position;
		}
		//Debug.Log ("origin "+ray.origin.x+","+ray.origin.y);
		//Debug.Log ("direction "+ray.direction.x+","+ray.direction.y);
		//Debug.Log ("result "+point.x+","+point.y);
		return new Vector3(point.x,point.y,0);
	}

	public void Initialize(){
		tentaclesHurt=new bool[lives];
        anim=GetComponent<Animator>();
		cuerpo = transform.Find ("cuerpo amarillo");
		maskAnim=cuerpo.Find("mask").GetComponent<Animator>();
        CalculateNextBlink();
		Transform tentaculos=transform.Find("cuerpo amarillo/tentaculos");
		tentacles=new Animator[lives];
		for (int i = 0,itop=tentacles.Length; i < itop; i++) {
			tentacles [i] = tentaculos.Find ("" + (i + 1) + "/t_" + (i + 1)).GetComponent<Animator> ();
		}
    }

    void CalculateNextBlink(){
        timeBeforeBlink = 0;
        nextBlink=Random.Range(1f, 10f);
    }

	public void CustomFixedUpdate(){
		if (!alive){
			return;
		}
		timeBeforeBlink += Time.deltaTime;
		if(timeBeforeBlink>=nextBlink){
			maskAnim.SetTrigger("blink");
			CalculateNextBlink();
		}
		if (Input.GetMouseButtonDown(0)){
			//Camera.main.ScreenToWorldPoint ();
			//Vector3 dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
			walkTarget=GetMouseVector();
			transform.right = walkTarget - transform.position;
			//transform.LookAt (walkTarget);
            //float angle = Mathf.Atan2(walkTarget.y, walkTarget.x) * Mathf.Rad2Deg;
            //transform.rotation = Quaternion.AngleAxis(angle-90f, Vector3.forward);
            //distance=Vector3.Distance(dir, transform.position)/500;
			if (!walking) {
				anim.SetTrigger("walk");
			}
            walking = true;
        }
        /*if(distance>0){
			float frameDistance=speed*Time.deltaTime;
			distance-=frameDistance;
			transform.Translate(0, frameDistance, 0);
		}else{
		}*/
        //optimize this and animate
        if (walking)
        {
            float frameDistance = speed * Time.deltaTime;
            float x=transform.position.x, y= transform.position.y, z= transform.position.z;
            transform.position = Vector3.MoveTowards(transform.position, walkTarget, frameDistance);
            float diffX= x - transform.position.x, diffY= y - transform.position.y, diffZ= z - transform.position.z;
            //cameraTransform.position = new Vector3(transform.position.x, transform.position.y, cameraTransform.position.z);
            //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.FromToRotation(transform.position, walkTarget - transform.position), Time.deltaTime);
            //transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, speed * Time.deltaTime);
            if ((diffX<0.001&& diffX > -0.001)&& (diffY < 0.001 && diffY > -0.001)&& (diffZ < 0.001 && diffZ > -0.001))
            {
                walking = false;
                anim.SetTrigger("stopWalk");
            }
        }
	}


    void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag != "Enemy") {
			return;
		}
        lives--;
        int index;
        //anim.Play("hurt");
        if (lives == 0) {
            anim.Play ("death");
            alive = false;
        } else {
            for (int i = 0; i < tentacles.Length; i++) {
                if (!tentaclesHurt [i]) {
                    tentacles [i].SetTrigger ("hurt");
                }
            }
            do{
                index= Random.Range(0, tentaclesHurt.Length);
            }while(tentaclesHurt[index]);
            tentaclesHurt [index] = true;
            tentacles [index].SetTrigger ("corrupt");
        }
    }

}
