﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Egg : MonoBehaviour {

    public float maxIncubationTime=3F;
    float currentIncubationTime=0F;
    public int arrayPos;
	public float size;

    void Start(){
        maxIncubationTime = Random.Range(1f, 10f);
    }

    public void CustomUpdate () {
        currentIncubationTime+=Time.deltaTime;
        if(currentIncubationTime>maxIncubationTime){
            Hatch();
        }
    }

    void Hatch(){
        EggManager.Hatch (new Vector3 (transform.position.x, transform.position.y, 0),arrayPos,size);
        Destroy(gameObject);
    }
}
