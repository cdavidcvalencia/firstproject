using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterManager {

    public static GameObject monsterPrefab;
	public static GameObject monsterPrefabMedium;
	public static GameObject monsterPrefabSmall;
    public static float smallChance=0;
    public static float mediumChance=0;
    static Enemy[] monsterInstances;

    public static void Initialize(int level,int levelMultiplier,int size){
		float delta = -(size/2);
		float deltaSize = delta + size;
		Vector2 pos;
		new Vector3 (Random.Range (delta, deltaSize), Random.Range (delta, deltaSize), 0);
        GameObject monster;
        if(monsterInstances!=null){
            for (int i = 0; i < level; i++){
                Object.Destroy(monsterInstances[i].gameObject);
            }
        }
        InitializeRoulette(level);
        monsterInstances = new Enemy[level*16];
        float monsterSize;
		float distance;
        Enemy enemy;
        for (int i = 0; i < level*8; i++){
            float chance=Random.Range(0f,100f);
            if(chance<smallChance){
                monster = Object.Instantiate(monsterPrefabSmall);
				monsterSize=0.25f;
            }else if(chance<(smallChance+mediumChance)){
                monster = Object.Instantiate(monsterPrefabMedium);
				monsterSize=0.5f;
            }else{
                monster = Object.Instantiate(monsterPrefab);
				monsterSize=1f;
            }
            enemy=monster.GetComponent<Enemy>();
            enemy.target = Globals.playerBody.transform;
			enemy.size=monsterSize;
            monsterInstances [i] = monster.GetComponent<Enemy> ();
            monsterInstances [i].arrayPos = i;
			do{
				pos=new Vector2(Random.Range (delta, deltaSize),Random.Range (delta, deltaSize));
				distance=Vector2.Distance(Vector2.zero,pos);
			}while(distance<0.35f&&distance>-0.35f);
			monster.transform.position = new Vector3 (pos.x, pos.y, 0);
        }
    }

    public static void InitializeRoulette(int level){
        float remainingChance=100f;
        if(level<20){
            smallChance=100f-((level-1)*5);
        }else{
            smallChance=5;
        }
        remainingChance-=smallChance;
        if(level<20){
            mediumChance=(level-1)*5;
        }else if(level<37){
            mediumChance=100f-((level-19)*5);
        }else{
            mediumChance=10;
        }
        remainingChance-=mediumChance;
    }

	public static void CreateMonster(Vector3 pos,float size){
		Enemy enemy;
		if(size<0.3f){
			enemy=Object.Instantiate(monsterPrefabSmall, pos, Quaternion.identity).GetComponent<Enemy>();
		}else if(size<0.6){
			enemy=Object.Instantiate(monsterPrefabMedium, pos, Quaternion.identity).GetComponent<Enemy>();
		}else{
			enemy=Object.Instantiate(monsterPrefab, pos, Quaternion.identity).GetComponent<Enemy>();
		}
		enemy.size = size;
        enemy.target=Globals.playerBody.transform;
        int freePos = -1;
        int numMonsterPositions = monsterInstances.Length;
        for (int i = 0; i < numMonsterPositions; i++) {
            if (monsterInstances [i] == null) {
                freePos = i;
                break;
            }
        }
        if (freePos == -1) {
            Enemy[] newMonsterInstances= new Enemy[numMonsterPositions*2];
            for (int i = 0; i < numMonsterPositions; i++) {
                newMonsterInstances [i] = monsterInstances [i];
            }
            freePos = numMonsterPositions;
            monsterInstances = newMonsterInstances;
        }
        monsterInstances [freePos] = enemy;
        enemy.arrayPos = freePos;
    }

    public static void CustomUpdate(){
        for (int i = 0, itop = monsterInstances.Length; i < itop; i++) {
            if (monsterInstances [i] != null) {
                monsterInstances [i].CustomUpdate ();
            }
        }
    }

}
