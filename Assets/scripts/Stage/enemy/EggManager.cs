﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggManager {

    public static GameObject eggPrefab;
    static Egg[] eggInstances= new Egg[1];

	public static void Hatch(Vector3 pos,int arrayPos,float size){
        eggInstances [arrayPos] = null;
        MonsterManager.CreateMonster (pos,size);
    }

	public static void LayEgg(Vector3 pos,float size){
        Globals.EggLayed ();
        Egg egg = Object.Instantiate (eggPrefab, pos, Quaternion.identity).GetComponent<Egg> ();
		egg.size = size;
        int freePos = -1;
        int numEggPositions = eggInstances.Length;
        for (int i = 0; i < numEggPositions; i++) {
            if (eggInstances [i] == null) {
                freePos = i;
                break;
            }
        }
        if (freePos == -1) {
            Egg[] newEggInstances= new Egg[numEggPositions*2];
            for (int i = 0; i < numEggPositions; i++) {
                newEggInstances [i] = eggInstances [i];
            }
            freePos = numEggPositions;
            eggInstances = newEggInstances;
        }
        eggInstances [freePos] = egg;
        egg.arrayPos = freePos;
    }

    public static void CustomUpdate(){
        for (int i = 0, itop = eggInstances.Length; i < itop; i++) {
            if (eggInstances [i] != null) {
                eggInstances [i].CustomUpdate ();
            }
        }
    }

}
