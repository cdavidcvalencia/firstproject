using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float speed = 0.01F;
    public Transform target;
    public float maxMovementTime=3F;
    float currentMovementTime=-1F;
    public float randomAnimationTime=0.1f;
    public float layEggPosition;
    public bool layEgg;
    public float idleTime;
    public Animator anim;
    public int arrayPos;
    public float size;
	float growTime;
	float elapsedGrowTime;
	public float layEggTime=2f;
	public float layEggCurrentTime=3f;
	public bool laying = false;

    void Start(){
        maxMovementTime = Random.Range(10f, 20f)*size;
        speed = Random.Range(0.1f,0.5f)*size;
		growTime = Random.Range(40f,120f)*size;
        anim.SetTrigger("startWalking");
		elapsedGrowTime = 0;
    }

    public void CustomUpdate () {
		if (laying) {
			if (layEggCurrentTime < layEggTime) {
				layEggCurrentTime += Time.deltaTime;
				return;
			} else {
				laying = false;
				anim.SetTrigger("startWalking");
			}
		}
		if(size<1f){
			elapsedGrowTime += Time.deltaTime;
			if(elapsedGrowTime > growTime){
				//stop
				//instantiate bigger
				//scale 2x (time.deltatime)
			}
		}
        if(currentMovementTime<0F){
            transform.right = target.position - transform.position;
            currentMovementTime=0;
            layEggPosition= maxMovementTime*Random.Range(0.2f, 0.8f);
            layEgg = Random.Range(0f, 100f) < Globals.layEggChance;
        }else{
            currentMovementTime+=Time.deltaTime;
            transform.Translate(speed*Time.deltaTime, 0, 0);
            if(layEgg&&currentMovementTime > layEggPosition )
            {
                layEgg = false;
                LayEgg ();
            }
            if(currentMovementTime>maxMovementTime){
                currentMovementTime=-1f;
            }
        }
    }

    void LayEgg(){
		laying = true;
		anim.SetTrigger("stopWalking");
		//anim.SetTrigger("layEgg");
		layEggCurrentTime = 0f;
        //stop some time while it lays the egg
		EggManager.LayEgg (new Vector3 (transform.position.x, transform.position.y, 0),size);
    }

}
