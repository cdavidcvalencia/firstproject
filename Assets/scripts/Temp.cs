﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temp : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		Debug.DrawRay (ray.origin,ray.direction, Color.red, 5);
		transform.LookAt (new Vector3(ray.origin.x,ray.origin.y,0));

		transform.localRotation = Quaternion.Euler (transform.rotation.eulerAngles.x,90,transform.rotation.eulerAngles.z);
	}
}
